// console.log("Siesta Time");

// [Array Method]

// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array elements.

// [section] Mutator Method
	// are functions that mutate or change an array after they're created
	// These methods maniplulate
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);
	
	// puch()
	/*
		-Adds an element/s in the end of an array and returns the array's length.

		-Syntax:
		arrayName.push(elementsToBeAdded);
	*/

console.log("Current Array fruits[]: ");
console.log(fruits);

// this fuctions mutates
let fruitsLength = fruits.push('Mango');
console.log("Mutated array from push method: ");
console.log(fruits);
console.log(fruitsLength);

// Adding multiple elements to an array.
fruitsLength = fruits.push("Avocado", "Guava");
console.log("Mutated array from push method: ");
console.log(fruits);
console.log(fruitsLength);


// Pop()
/*
	-Removes the last element in an array and return the removed elemet

	Syntax:
	arrayName.pop();
*/

console.log("Current Array fruits[]: ");
console.log(fruits);

let removedFruit = fruits.pop();
console.log("Mutated array after the pop method: ");
console.log(fruits);
console.log(removedFruit);

console.log("Current Array fruits[]: ");
removedFruit = fruits.pop();
console.log("Mutated array after the pop method: ");
console.log(fruits);
console.log(removedFruit);

// unshift()
/*
	-Addsone or more elements at the beggining of an array. and returns the present length.
	-Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

console.log("Current Array fruit[]:")
console.log(fruits);

fruitsLength = fruits.unshift('Lime', 'Banana');
console.log("Mutated Array after the unshift() method: ");
console.log(fruits);
console.log(fruitsLength);

// shift()
/*
	-removes an element at the beginning of an array and it returns the removed element.
	-Syntax:
		arrayName.shift();
*/

console.log("Current Array fruits[]: ");
console.log(fruits);
removedFruit = fruits.shift();

console.log("Mutated Array after the shift() method: ");
console.log(fruits);
console.log(removedFruit);

// splice()
	// Simultaneously removes elements from a specific index number and adds elements.
	/*
		arrayName.splice(startingIndex, deleteCount, elementsToBeDeeleted)
	*/

console.log("Current Array fruits[]: ");
console.log(fruits);

fruits.splice(1, 1, "Lime")
console.log(fruits);
console.log("Mutated Array after the splice() method: ");
console.log(fruits);
/*adding without removing
fruits.splice(5,0, "Cherry");
console.log(fruits);*/

// Removing index elemets
console.log(fruits);
console.log (fruits.splice(3,1));
console.log(fruits);

console.log("Current Array fruits[]: ");
console.log(fruits);

fruits.splice(3, 0, "Durian", "Santol");
console.log("Mutated Array after the splice() method: ");
console.log(fruits);


//  Sort()
	/*
		-Rearranges the array elements in alphanumeric order
		-syntax:
		arrayName.sort();
	*/


console.log("Current Array fruits[]: ");
console.log(fruits);
console.log (fruits.sort());

console.log("Mutated Array after the sort() method: ");
console.log(fruits);

/*
	Important Note:
	The sort method is used formore comlicated functions fucos the bootcamperson the basic usage of the sort method.
*/

// reverse()
	/*
		-reverses the order of array elements
		syntax:
			arrayName.reverse();
	*/

console.log("Current Array fruits[]: ");
console.log(fruits);
console.log("Returns of reverse() method: ");
fruits.reverse();
console.log (fruits.reverse());

console.log("Mutated Array after the reverse() method: ");
console.log(fruits);


// [section] Non-mutator Method

/*
	-Non-mutator methods re functions that do not modify or chang an array after the're created.

	-These methods do not manipulatethe original array performing various taskssuchs as returnining elements from an array and combining arrays and pringting the output
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE', 'PH'];

	// indexOf()
	/*
		-it returns the index number of the first matching elemnt found in an array
		-if no match was found, the result will be -1
		-The search process will be done from first element proceeding to the last element.
		Syntax: arrayName.indexOf(searchValue);
				arrayName.indexOf(searchValue, startingIndex);
	*/

console.log(countries.indexOf('PH'));
console.log(countries.indexOf('BR'));

/*arrayName.indexOf(searchValue, startingIndex);*/
console.log(countries.indexOf('PH', 2));

// lastIndexOf()
/*
	-return the index number of the last mtching element found in n array
	-the search process will be done from last element proceedinng to the first element.

	Syntax:
	arrayName.lastIndexOf(searchValue)
	arrayName.lastIndexOf(searchValue, startingIndex)
*/

console.log(countries.lastIndexOf('PH'));

// slice()
/*
	-portion/slices from and array and return a new array
	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex,endingIndex)
*/

// Slicing off elements from a pspecified index to the last element
let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);
console.log(countries);

// Slicing off elements from a specified index to another index.

let slicedArrayB = countries.slice(1,5);
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array

let slicedArrayC = countries.slice(-3, -1);
console.log(slicedArrayC);

// toString()
/*
	-returns an array as astring separated by commas
	-syntax
	arrayName.toString().
*/
let stringedArray = countries.toString();
console.log(stringedArray);

// concat()
/*
	combines two arrays and returns the combined results
	-Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let tasksArrayA = ['drink HTML', 'eat javascript'];
let tasksArrayB = ['inhale CSS', 'breathe SAAS'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('result from concat method: ');
console.log(tasks);


//combining multiple arrays
console.log("Result from concat method: ");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combine arrays with specific elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log("Resuls from concat method: ");
console.log(combinedTasks);

// join()
/*
	-returns array as string separated by specified separator string
	-syntax:
	arrayName.join('separatorString');
*/
let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' - '));


// [section] Itertion Methors
/*
	Iteration methods are loop designed to perform repetitive tasks on arrays
	Iteration method loops over all elements in array.
*/

// forEach()
/*
	-similar to a for loop that iterates on each of element
	-for eah element in the array, the function in for each method will be run
	arrayName.forEach(function(indivElement)){
	statement/s;
	}
*/

console.log(allTasks);

allTasks.forEach(function(task){
	console.log(allTasks);
})

let filteredTasks = [];
let task = allTasks.forEach(function(task){
	if (task.length > 10) {
		filteredTasks.push(task);
	}

})
console.log(task);
console.log(filteredTasks);

// map()
/*
	-it iterates on each element and returns new arrays different values depending on the result of the function's operation.
	-Syntax:
		let/const resultArray = arrayname.map(function(elements){
		statements;
		return;
		})
*/

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number*number;
})

console.log("Original Array: ");
console.log(numbers);
console.log("result of map metho: ");
console.log(numberMap);

// every()
/*
	checks if all elements in an array meet the given condition
	-this is useful validating data stored in arrays. especially when dealing with large amounts of data.
	-returns a true value if all elements meet the condition and false if otherwise.

	syntax:
	let/const resultArray = arrayName.every(function (element){
		return expression/condition;
	})
*/

console.log(numbers);
let allValid = numbers.every(function(number){
	// return (number<3); - false
	// True:
	return (number<6);
})
console.log(allValid);

// some()
/*
	-checks if at least one element in the array meet the given condition;
	-returns a true value if at least one element meets the condition and falseif none.

	-syntax:
		let/const resultArray = arrayName.some(function(elements){
			return expression/condition
		})
*/

console.log(numbers);
let someValid = numbers.some(function(number){
	return (number > 5 );
})
console.log(someValid);

// filter()
/*
	- return new array that contains the elements which meets the given condition
	- return an empty array if no element/s were found.

	syntax:
	let/const resultArray = arrayName.filter(function(element){
		return expression/condition;
	})
*/

console.log(numbers);
let filterValid = numbers.filter(function(number){
	// return (number < 3);
	return (number === 0);
})
console.log (filterValid);

// includes()
/*
	-includes() checks if the argument passed can be found in the array.
	-it returns boolean which can be save in variable 
		-returns true if the argument is found in the array.
		-returns falsie if it is not.

		syntax:
			arrayName.include(argument);
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes('Mouse');
console.log(productFound1);

let productFound2 = products.includes('Headset');
console.log(productFound2);

// reduce();
/*
	-evaluate elements from left to right and returns/ reduces the array into a sinle value.
	syntax:
	let/const resultValue = arrayName.(reduce(function(accumulator, currentValue)){
		return expression / operation
	});
*/

console.log(numbers);
let total = numbers.reduce(function(x,y){
	console.log("This is the value of x: "+x);
	console.log("This is the value of y: "+y);
	return x+y;
})

	console.log(total);